import React from 'react';
import './Footer.scss';

const Footer = () => {
    return (
        <div className="Footer">
            <a href="https://www.clark.de/de/start2"
               target="__blank"
               title="Visit Clark Website"
               className="Footer__Link">

            </a>
        </div>
    );
};

export default Footer;
