import React, { Component } from 'react';

// components
import Questionnaire from '../Questionnaire';
import Footer from '../Footer';

// styles
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Questionnaire />
        <Footer />
      </div>
    );
  }
}

export default App;
