import React from 'react';

// styles
import './Loader.scss';

const Loader = () => (
    <div className="Loader">Loading...</div>
);

export default Loader;
